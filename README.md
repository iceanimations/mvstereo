# What is it #

This script enables creation of a stereoscopic camera rig of 9 cameras used in creating content for autostereoscopic displays

## Setup

1. unpack the repository in your site directory in a directory named `mvstereo`
2. Then within maya script editor enter the following

```
#!python

import mvstereo
mvstereo.registerMultiRig()
```

Now a `multiStereoRig` can be generated using `panels -> stereo` menu in the panel editor.